# LuxEvent Project

This is a project done using NestJS Universal with Angular.

LuxEvent consists of a project that will help people in Luxembourg create and search for events in their area.



## Getting Started

### Installation

- `npm i`

### Development (Client-side only rendering)

- `npm start` which will run `ng serve`.

### Development (Server-side rendering)

- `npm run dev:ssr`.

### Production

\*`npm run build:ssr && npm run serve:ssr`

- Compiles your application and spins up a Nest server to serve your Universal application on `http://localhost:4000`.

\*`npm run prerender`

- Compiles your application and prerenders your applications files

# License

[![MIT License](https://img.shields.io/badge/license-MIT-blue.svg?style=flat)](/LICENSE)
