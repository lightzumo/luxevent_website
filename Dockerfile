FROM node:latest
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY . .
RUN npm run build:ssr
EXPOSE 4200
ENV PORT="4200"
ENV VIRTUAL_HOST="luxevent.wsilva.fun"
ENV LETSENCRYPT_EMAIL="wsilva.pro@gmail.com"
ENV LETSENCRYPT_HOST="luxevent.wsilva.fun"
ENV DBPORT="3306"
ENV DBHOST="172.17.0.4"
ENV DBUSER="root"
ENV DBPASSWORD="schoolpw"
ENV DBNAME="dbLuxEvent"
CMD [ "node", "dist/LuxEvent/server/main.js" ]
