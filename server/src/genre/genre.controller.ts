import {Body, Controller, Get, Headers, Post, UseGuards} from '@nestjs/common';
import {GenreService} from "./genre.service";
import {AuthGuard} from "@nestjs/passport";

@Controller('genre')
export class GenreController {

    constructor(private readonly genreService: GenreService) { }

    //Returs all genres
    @Get('all')
    public async  getAllGenres(){
        return await this.genreService.getAllGenres()
    }

    //Creates an Event
    @Post('manage/create')
    @UseGuards(AuthGuard('jwt'))
    public async createGenre(@Body('description') description: string, @Headers() headers){
        return await this.genreService.createGenre(description, headers.authorization);
    }
}
