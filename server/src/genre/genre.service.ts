import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {dbInformation} from "../helpFunctions";
import * as JWT from 'jwt-decode';

@Injectable()
export class GenreService {

    //Returns all Kinds of events (Genres)
    public async getAllGenres(){

        const mysql = require('mysql2/promise');
        const connection =  await mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        try {
            const [err, result, fields] = await connection.execute('SELECT * from tblGenre;');

            if (err.length < 1){
                throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
            }
            let genres = [];

            for( let i = 0; i < err.length; i++ ){
                genres.push({
                    GenreId : err[i].idGenre,
                    GenreName: err[i].dtTitle
                })
            }
            connection.end();
            return {category: genres};
        } catch {
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Creates a kind of event (Genre)
    public async createGenre(name: string, token: any){

        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        if (token.role !== 'admin'){
            throw new HttpException('You do not have the right to create a genre.', HttpStatus.UNAUTHORIZED);
        }


        //Initializes the variables to connect to the db
        const mysql = require('mysql2/promise');
        const connection =  await mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        try {
            //Genre gets created
            const [err, result, fields] = await connection.execute('call sp_createGenre(?);',[name]);

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify({ done: true, message : 'Genre created!' });
        } catch (e){
            console.log(e);
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    };

}
