//DTO of an event
export interface eventDto {
    id: number;
    title: string;
    startDate: string;
    endDate: string;
    price: any;
    address: string;
    description: string;
    longitude: number;
    latitude: number;
    image: string;
    category: number;
    creator: number;
}
