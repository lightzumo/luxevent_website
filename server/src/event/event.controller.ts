import {
    Body,
    Controller,
    Post,
    Request,
    UploadedFile,
    UseGuards,
    UseInterceptors,
    Headers,
    Get,
    Param, Query
} from '@nestjs/common';
import {FileInterceptor, FilesInterceptor} from "@nestjs/platform-express";
import { extname } from 'path';
import { diskStorage } from 'multer';
import {RegisterDto} from "../auth/auth.dto";
import {eventDto} from "./event.dto";
import {AuthGuard} from "@nestjs/passport";
import {editFileName, imageFileFilter} from "../helpFunctions";
import {EventService} from "./event.service";

@Controller('event')
export class EventController {

    constructor(private eventService: EventService) { }

    //Creates an event
    // src: https://medium.com/better-programming/nestjs-file-uploading-using-multer-f3021dfed733
    @Post('manage/create')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(
        FileInterceptor('image', {
            storage: diskStorage({
                destination: './dist/LuxEvent/server/files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    public async createEvent(@UploadedFile() file, @Headers() headers, @Body() event: eventDto) {
        return this.eventService.createEvent(event, headers.authorization, file);
    }


    //edit data of an existing event
    @Post('manage/edit')
    @UseGuards(AuthGuard('jwt'))
    @UseInterceptors(
        FileInterceptor('image', {
            storage: diskStorage({
                destination: './dist/LuxEvent/server/files',
                filename: editFileName,
            }),
            fileFilter: imageFileFilter,
        }),
    )
    public async editEvent(@UploadedFile() file, @Headers() headers, @Body() event: eventDto) {
        return this.eventService.editEvent(event, headers.authorization, file);
    }


    //Returns a specific event
    @Get('get/:id')
    public async getEvent(@Param() params){
        return this.eventService.getEventById(params.id);
    }

    //Returns if user is going to event or not
    @Get('user/like/:id')
    @UseGuards(AuthGuard('jwt'))
    public async isUserGoingEvent(@Headers() headers, @Param() params){
        return this.eventService.doesUserLikeEvent(headers.authorization, params.id);
    }


    //Makes user going or not going to event
    @Post('user/like')
    @UseGuards(AuthGuard('jwt'))
    public async userGoing(@Headers() headers, @Body('event') eventId: number){
        return this.eventService.userGoingToEvent(headers.authorization, eventId);
    }


    //Search events
    @Get('search')
    public async searchEvents(@Query() query){
        return  this.eventService.searchEvent(query.date, query.genre);
    }

    //Search events by date
    @Get('searchByDate')
    public async searchEventsByDate(@Query() query){

        return  this.eventService.searchEventByDate(query.date);
    }


    //Search events
    @Get('search/iOS')
    public async searchEventsForiOS(@Query() query){
        return  this.eventService.searchEventForiOS(query.date, query.genre);
    }

    //Search events by date
    @Get('searchByDate/iOS')
    public async searchEventsByDateForiOS(@Query() query){
        return  this.eventService.searchEventByDateForiOS(query.date);
    }


    //Returns all events which the user created
    @Get('my')
    @UseGuards(AuthGuard('jwt'))
    public async getUsersEvents(@Headers() headers){
        return  this.eventService.searchAllUsersEvents(headers.authorization);
    }

    //Returns all events which the user created
    @Get('all')
    @UseGuards(AuthGuard('jwt'))
    public async getAllEvents(@Headers() headers){
        console.log('hehe');
        return  this.eventService.searchAllEvents(headers.authorization);
    }


    //Returns all events which the user created
    @Get('attend')
    @UseGuards(AuthGuard('jwt'))
    public async getUsersAttendEvents(@Headers() headers){
        return  this.eventService.searchAllUsersAttendEvents(headers.authorization);
    }

    //returns todays events
    @Get('today')
    public async getTodayEvent(){
        return  this.eventService.getEventsOfToday();
    }


}
