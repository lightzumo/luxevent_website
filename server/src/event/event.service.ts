import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import {eventDto} from "./event.dto";
import {dbInformation} from "../helpFunctions";
import * as JWT from 'jwt-decode';
import {HttpErrorResponse} from "@angular/common/http";
import * as fs from "fs";

@Injectable()
export class EventService {
    mysql = require('mysql2/promise');

    //Function to create an event-details in the DB
    public async createEvent(event: eventDto, token: any, file: any) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        //Initializes the variables to connect to cloudinary
        const cloudinary = require('cloudinary').v2;
        cloudinary.config({
            cloud_name: 'df6n0acuy',
            api_key: '198139659897914',
            api_secret: '7exVo7TmRZfxEF22deUdz_f4voI'
        });



        if (file !== undefined){
            let result;
            // File upload (example for promise api)
            await cloudinary.uploader.upload(__dirname + "/files/" + file.filename)
                .then(function (image) {
                    result = image.url;
                })
                .catch(function (err) {
                    result = 'https://res.cloudinary.com/df6n0acuy/image/upload/v1585660861/defaultImage_bvksgs.jpg'
                });

                let {title, startDate, endDate, price, address, description, longitude, latitude, category, image} = event;

                image = result;


                //Controls if price is defined or not/ if not it means its free
                if (price === undefined) {
                    price = null;
                }
                try {
                    //Executes the stored procedure in the DB to create an event-details
                    await connection.execute(
                        'call sp_createEvent(?,?,?,' + price + ',?,?,?,?,?,?,?);',
                        [title, startDate, endDate, address, description, longitude, latitude, image, category, token.id]
                    );

                    //Closes db connections
                    connection.end();

                    //Deletes old image
                    try {
                        fs.unlinkSync(__dirname + "/files/" + file.filename);
                    } catch(err) {
                        console.error(err);
                    }
                    //return a success message to user
                    return JSON.stringify({done: true, message: 'Event created!'});
                } catch {
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }else {

                let {title, startDate, endDate, price, address, description, longitude, latitude, category, image} = event;

                 //default image
                image = 'https://res.cloudinary.com/df6n0acuy/image/upload/v1585660861/defaultImage_bvksgs.jpg';

                //Controls if price is defined or not/ if not it means its free
                if (price === undefined) {
                    price = null;
                }

                try {
                    //Executes the stored procedure in the DB to create an event-details
                    await connection.execute(
                        'call sp_createEvent(?,?,?,' + price + ',?,?,?,?,?,?,?);',
                        [title, startDate, endDate, address, description, longitude, latitude, image, category, token.id]
                    );

                    //Closes db connections
                    connection.end();

                    //return a success message to user
                    return JSON.stringify({done: true, message: 'Event created!'});
                } catch {
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
        }
    }


    //returns events by its ID
    public async getEventById(id :number){
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE idEvent = ?;',
                [id]
            );

            if (err.length < 1){
                throw new HttpException('Event not found', HttpStatus.NOT_FOUND);
            }

            let event: eventDto = {
                id: err[0].idEvent,
                title : err[0].dtTitle,
                startDate: err[0].dtStartDate,
                endDate: err[0].dtEndDate,
                price: err[0].dtPrice,
                address: err[0].dtAddress,
                description: err[0].dtDescription,
                longitude: err[0].dtLong,
                latitude: err[0].dtLat,
                image: err[0].dtImage,
                category: err[0].fiCategory,
                creator:  err[0].fiCreator
            };

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(event);
        } catch{

            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Returns if user likes an event or not
    public async doesUserLikeEvent(token: any, eventId: number){
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblGoingTo WHERE fiEvent = ? AND fiUSER = ?;',
                [eventId, token.id]
            );

            //Closes db connections
            connection.end();
            if (err.length < 1){
                return JSON.stringify({liked: false});
            }else {
                return JSON.stringify({liked: true});
            }
        } catch{
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //Returns if user likes an event or not
    public async userGoingToEvent(token: any, eventId: number){
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        try {
            //Executes the stored procedure in the DB to create an event-details
            let [err, result, fields] = await connection.execute(
                'SELECT * FROM tblGoingTo WHERE fiEvent = ? AND fiUSER = ?;',
                [eventId, token.id]
            );

            if (err.length < 1){
                let [erro, result, fields] = await connection.execute(
                    'call sp_GoingToEvent(?,?)',
                    [eventId, token.id]
                );

                if (erro.affectedRows === 0){
                    return JSON.stringify({ done: false});
                }else {
                    return JSON.stringify({ done: true });
                }
            }else {
                let [erro, result, fields] = await connection.execute(
                    'call sp_UpdateGoingToEvent(?,?)',
                    [eventId, token.id]
                );

                if (erro.affectedRows === 0){
                    return JSON.stringify({ done: false});
                }else {
                    return JSON.stringify({ done: true });
                }
            }
        } catch{
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // Function to search events by kind and startdate and its gets sorted by date
    public async searchEvent(date:any, genre:number) {
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE fiCategory = ? AND dtStartDate >= ? ORDER BY dtStartDate;',
                [genre, date]
            );

            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }

            let events: eventDto[]= [];
            for (let i = 0; i < err.length; i++){
                const eventToSend: eventDto = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: err[i].dtStartDate,
                    endDate: err[i].dtEndDate,
                    price: err[i].dtPrice,
                    address: err[i].dtAddress,
                    description: err[i].dtDescription,
                    longitude: err[i].dtLong,
                    latitude: err[i].dtLat,
                    image: err[i].dtImage,
                    category: err[i].fiCategory,
                    creator: err[i].fiCreator
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(events);
        } catch{
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Function to search events startdate and its gets sorted by date
    public async searchEventByDate(date:any) {
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE dtStartDate >= ? ORDER BY dtStartDate;',
                [date]
            );

            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }

            let events: eventDto[]= [];
            for (let i = 0; i < err.length; i++){
                const eventToSend: eventDto = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: err[i].dtStartDate,
                    endDate: err[i].dtEndDate,
                    price: err[i].dtPrice,
                    address: err[i].dtAddress,
                    description: err[i].dtDescription,
                    longitude: err[i].dtLong,
                    latitude: err[i].dtLat,
                    image: err[i].dtImage,
                    category: err[i].fiCategory,
                    creator: err[i].fiCreator
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(events);
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    // Function to search all events created by the user
    public async searchAllUsersEvents(token:any) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE fiCreator = ? ORDER BY dtStartDate DESC;',
                [token.id]
            );

            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }

            let events: any = [];
            for (let i = 0; i < err.length; i++){
                const eventToSend: any = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: err[i].dtStartDate,
                    endDate: err[i].dtEndDate,
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(events);
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }




    // Function to search all events
    public async searchAllEvents(token:any) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);


        if (token.role !== 'admin'){
            throw new HttpException('You are not allowed to access this data!', HttpStatus.NOT_FOUND);
        }

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent ORDER BY dtStartDate DESC;'
            );

            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }

            let events: any = [];
            for (let i = 0; i < err.length; i++){
                const eventToSend: any = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: err[i].dtStartDate,
                    endDate: err[i].dtEndDate,
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(events);
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //Function to edit event-details in the DB
    public async editEvent(event: eventDto, token: any, file: any) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        //Initializes the variables to connect to cloudinary
        const cloudinary = require('cloudinary').v2;
        cloudinary.config({
            cloud_name: 'df6n0acuy',
            api_key: '198139659897914',
            api_secret: '7exVo7TmRZfxEF22deUdz_f4voI'
        });

        //If image was updated upload new image to server
        if (file !== undefined){
            await cloudinary.uploader.upload(__dirname + "/files/" + file.filename)
                .then(function (image) {
                    event.image = image.url;
                    //Deletes old image
                    try {
                        fs.unlinkSync(__dirname + "/files/" + file.filename);
                    } catch(err) {
                        console.error(err);
                    }
                })
                .catch(async function (err) {
                    const  oldEvent = JSON.parse(await this.getEventById(event.id));
                    event.image = oldEvent['image'];
                });
        }else {
            const  oldEvent = JSON.parse(await this.getEventById(event.id));
            event.image = oldEvent['image'];

        }
        let {id, title, startDate, endDate, price, address, description, longitude, latitude, category, image} = event;


        //Controls if price is defined or not/ if not it means its free
        if (price === undefined){
            price = null;
        }

        try {
            //Selects the event again and control if the user that is trying to
            if (token.role !== 'admin'){
                const [err, result, fields] = await connection.execute(
                    'SELECT fiCreator FROM tblEvent WHERE idEvent = ?;',
                    [id]
                );

                if (err[0].fiCreator !== token.id){
                    //returns an error to the user
                    throw new HttpException('You are not allowed to edit this event', HttpStatus.UNAUTHORIZED);
                }
            }

            //Upadates the event-details
            await connection.execute(
                'UPDATE tblEvent SET dtTitle= ?, dtStartDate=?, dtEndDate=?, dtPrice= '+price+', dtAddress=?, dtDescription=?, dtLong=?, dtLat=?, dtImage=?, fiCategory=? WHERE (idEvent=?);',
                [title, startDate, endDate, address, description, longitude, latitude, image, category, id]
            );

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify({ done: true, message : 'Event successfully updated!' });
        } catch{
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    //returns today events
    public async getEventsOfToday() {
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        const todayDate = new Date();
        //Formats the dates to send to the server
        const formatted_startDate =
            todayDate.getFullYear() + '-' + (todayDate.getMonth() + 1) + '-' + todayDate.getDate() + ' 00:00:00';

        const tomorrowsDate = new Date(todayDate);
        tomorrowsDate.setDate(tomorrowsDate.getDate() + 1);
        //Formats the dates to send to the server
        const formatted_endDate =
            tomorrowsDate.getFullYear() + '-' + (tomorrowsDate.getMonth() + 1) + '-' + tomorrowsDate.getDate() + ' 00:00:00';

        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE dtStartDate >= ? AND dtStartDate <= ? ORDER BY dtStartDate;',
                [formatted_startDate, formatted_endDate]
            );

/*            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }*/

            let events: eventDto[]= [];
            for (let i = 0; i < err.length; i++){

                const startDate = new Date(err[i].dtStartDate);
                const endDate = new Date(err[i].dtEndDate);

                //Formats the dates to send to the iOS app
                const formatted_startDate =
                    startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() +  ' '
                    + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds();

                const formatted_endDate =
                    endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() +  ' '
                    + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds();

                const eventToSend: eventDto = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: formatted_startDate,
                    endDate: formatted_endDate,
                    price: err[i].dtPrice,
                    address: err[i].dtAddress,
                    description: err[i].dtDescription,
                    longitude: err[i].dtLong,
                    latitude: err[i].dtLat,
                    image: err[i].dtImage,
                    category: err[i].fiCategory,
                    creator: err[i].fiCreator
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return {events : events};
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }








    // Function to search events by kind and startdate and its gets sorted by date
    public async searchEventForiOS(date:any, genre:number) {
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE fiCategory = ? AND dtStartDate >= ? ORDER BY dtStartDate;',
                [genre, date]
            );

/*            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }*/

            let events: eventDto[]= [];
            for (let i = 0; i < err.length; i++){

                const startDate = new Date(err[i].dtStartDate);
                const endDate = new Date(err[i].dtEndDate);

                //Formats the dates to send to the iOS app
                const formatted_startDate =
                    startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() +  ' '
                    + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds();

                const formatted_endDate =
                    endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() +  ' '
                    + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds();

                const eventToSend: eventDto = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: formatted_startDate,
                    endDate: formatted_endDate,
                    price: err[i].dtPrice,
                    address: err[i].dtAddress,
                    description: err[i].dtDescription,
                    longitude: err[i].dtLong,
                    latitude: err[i].dtLat,
                    image: err[i].dtImage,
                    category: err[i].fiCategory,
                    creator: err[i].fiCreator
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return {events : events};
        } catch{
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    // Function to search events start date and its gets sorted by date
    public async searchEventByDateForiOS (date:any) {
        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'SELECT * FROM tblEvent WHERE dtStartDate >= ? ORDER BY dtStartDate;',
                [date]
            );


            let events: eventDto[]= [];
            for (let i = 0; i < err.length; i++){

                const startDate = new Date(err[i].dtStartDate);
                const endDate = new Date(err[i].dtEndDate);

                //Formats the dates to send to the iOS app
                const formatted_startDate =
                    startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() +  ' '
                    + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds();

                const formatted_endDate =
                    endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() +  ' '
                    + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds();

                const eventToSend: eventDto = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: formatted_startDate,
                    endDate: formatted_endDate,
                    price: err[i].dtPrice,
                    address: err[i].dtAddress,
                    description: err[i].dtDescription,
                    longitude: err[i].dtLong,
                    latitude: err[i].dtLat,
                    image: err[i].dtImage,
                    category: err[i].fiCategory,
                    creator: err[i].fiCreator
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return {events : events};
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }





    // Function to search all events created by the user
    public async searchAllUsersAttendEvents(token:any) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        const todayDate = new Date();
        //Formats the dates to send to the server
        const formatted_startDate =
            todayDate.getFullYear() + '-' + (todayDate.getMonth() + 1) + '-' + todayDate.getDate() + ' 00:00:00';


        try {
            //Executes the stored procedure in the DB to create an event-details
            const [err, result, fields] = await connection.execute(
                'Select * from tblEvent, tblGoingTo  WHERE idEvent = fiEvent AND fiUser = ? AND dtEndDate > ?;',
                [token.id, formatted_startDate]
            );

            if (err.length < 1) {
                throw new HttpException('No Events found!', HttpStatus.NOT_FOUND);
            }

            let events: any = [];
            for (let i = 0; i < err.length; i++){
                const eventToSend: any = {
                    id: err[i].idEvent,
                    title: err[i].dtTitle,
                    startDate: err[i].dtStartDate,
                    endDate: err[i].dtEndDate,
                };
                events.push(eventToSend);
            }

            //Closes db connections
            connection.end();

            //return a success message to user
            return JSON.stringify(events);
        } catch {
            //returns an error to the user
            throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
