import {Body, Controller, Get, Header, Post, UseGuards, Request} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import {AuthService} from "./auth.service";
import * as crypto from 'crypto';
import {RegisterDto} from "./auth.dto";
@Controller('auth')
export class AuthController {

    constructor(private readonly authService: AuthService) {}
    @Post('register')
   public async createAccount(@Body() register: RegisterDto){
        //create a random salt and then encrypt the password
        const salt: string = crypto.randomBytes(32).toString('hex');
        const encryptPassword: string = crypto.pbkdf2Sync(register.password, salt, 10000, 100, 'sha512').toString('hex');

        //Formats the birth date
        const date = new Date(register.birthday);
        const formattedDate =  String(date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate());
        register.birthday = formattedDate;
        register.password = encryptPassword;
        return await this.authService.insertAccount(register, salt, 'user');
    };

    //Logs the user in
    @UseGuards(AuthGuard('local'))
    @Post('login')
    public async login(@Request() req) {
        return this.authService.login(req.user);
    }

}
