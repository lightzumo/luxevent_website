import {Module} from '@nestjs/common';
import {UserModule} from '../user/user.module';
import {PassportModule} from '@nestjs/passport';
import {AuthService} from './auth.service';
import {JwtModule} from '@nestjs/jwt';
import {JwtStrategy} from './jwt.strategy';
import {LocalStrategy} from './local.strategy';
import {AuthController} from './auth.controller';
import { jwtSecret} from "../helpFunctions";

@Module({
    imports: [
        UserModule,
        PassportModule,
        JwtModule.register({
            secret: jwtSecret,
            signOptions: {expiresIn: '7 days'},
        }),
    ],
    providers: [AuthService, LocalStrategy, JwtStrategy],
    exports: [AuthService],
    controllers: [AuthController],
})
export class AuthModule {
}
