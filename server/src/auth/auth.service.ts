import {Injectable} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

import {UserService} from "../user/user.service";
import {RegisterDto} from "./auth.dto";
import {dbInformation} from "../helpFunctions";

@Injectable()
export class AuthService {
    constructor(private  usersService: UserService, private readonly jwtService: JwtService) {
    }

    //Creates an account
    public async insertAccount(registerDto: RegisterDto, salt: String, role: String) {
        let done: boolean = false;
        const mysql = require('mysql2/promise');
        const connection =  await mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        //check if email exists
        const [result] = await connection.execute('SELECT * from tblUser where dtEmail =?',[registerDto.email]);
        if (result.length > 0){
            return JSON.stringify({ done: done , message : 'Email exists!'});
        }

        //create User
        const [err] = await connection.execute('call sp_createUser(?,?,?,?,?,?,?)',
            [registerDto.name, registerDto.birthday, registerDto.password, registerDto.email, salt, role, '']);
        if (err.affectedRows === 0){
            done = false;
            return JSON.stringify({ done: done, message : 'Error creating the account!' });
        }else {
            done = true;
            return JSON.stringify({ done: done, message : 'Account created!' });
        }

    }

    //Validates an user if login correct
    async validateUser(emailAddress: string, password: string){
        return await this.usersService.findByLogin({ emailAddress, password });
    }

    //Returns a token to the user that logged in
    async login(user: any) {
        const payload = { email: user.email, id: user.id, role: user.role, name: user.name};
        return {
            access_token: this.jwtService.sign(payload),
        };
    }
}
