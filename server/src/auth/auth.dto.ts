import {Body} from "@nestjs/common";

export interface LoginDto {
    dtEmail: string;
    dtPassword: string;
}

export interface RegisterDto {
    name:string;
    email:string;
    password:string;
    birthday:any;
}
