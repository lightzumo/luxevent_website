
import {extname} from "path";

// Information to connect to DB
export const dbInformation = {
        host: process.env.DBHOST || 'localhost',
        user: process.env.DBUSER || 'root',
        database: process.env.DBNAME || 'dbLuxEvent',
        password: process.env.DBPASSWORD || 'qwertzui'
};


//JWT Secret Key
export const jwtSecret = 'secret';


//Creates a new filename with the original filename with 9 random characters
export const editFileName = (req, file, callback) => {
    const name = file.originalname.split('.')[0];
    const fileExtName = extname(file.originalname);
    const randomName = Array(9)
        .fill(null)
        .map(() => Math.round(Math.random() * 16).toString(16))
        .join('');
    callback(null, `${name}-${randomName}${fileExtName}`);
};

//Checks if file is as image or not
export const imageFileFilter = (req, file, callback) => {
    if (!file.originalname.match(/\.(jpg|jpeg|png)$/)) {
        return callback(new Error('Only image files are allowed!'), false);
    }
    callback(null, true);
};

