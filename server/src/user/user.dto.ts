//DTO of a user
export interface UserDto {
    id: number;
    Name: string;
    BirthDate: any;
    Email: string;
    Password: string,
    Salt: string,
    Role: string
}
