import {Body, Controller, Get, Headers, Param, Post, UseGuards} from '@nestjs/common';
import {AuthGuard} from "@nestjs/passport";
import {UserService} from "./user.service";

import {UserDto} from "./user.dto";


@Controller('user')
export class UserController {
    constructor(private readonly userService: UserService) { }

    //Updates a profile
    @Post('manage/edit/:id')
    @UseGuards(AuthGuard('jwt'))
    public async updateProfile(@Body() user: UserDto, @Headers() headers, @Param() params){
        //Formats the birth date
        const date = new Date(user.BirthDate);
        const formattedDate =  String(date.getFullYear() + "-" + (date.getMonth()+1) + "-" + date.getDate());
        user.BirthDate = formattedDate;
        return await this.userService.updateProfile(user, headers.authorization, params.id);
    }


    //Returns a profile of a user
    @Get('manage/get/:id')
    @UseGuards(AuthGuard('jwt'))
    public async getUserProfile(@Headers() headers, @Param() params){
        return await this.userService.getUserProfile(params.id, headers.authorization);
    }


    //Returns a profile of a user
    @Get('manage/all')
    @UseGuards(AuthGuard('jwt'))
    public async getAllUserProfile(@Headers() headers){
        return await this.userService.getAllUserProfile(headers.authorization);
    }
}
