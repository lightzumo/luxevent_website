import {HttpException, HttpStatus, Injectable} from '@nestjs/common';
import * as crypto from 'crypto';
import {dbInformation} from "../helpFunctions";
import {eventDto} from "../event/event.dto";
import * as fs from "fs";
import {UserDto} from "./user.dto";
import * as JWT from 'jwt-decode';

@Injectable()
export class UserService {

    mysql = require('mysql2/promise');
    // this is a function to find a user by login
    public async findByLogin(userDTO: any){
        const { emailAddress, password } = userDTO;
        const mysql = require('mysql2/promise');
        const connection =  await mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        //check if email exists
        const [err, result, fields] = await connection.execute('SELECT * from tblUser where dtEmail =?',[emailAddress]);

        if (err.length !== 1){
            throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
        }

       const hash = crypto.pbkdf2Sync(password, err[0].dtSalt, 10000, 100, 'sha512').toString('hex');
        if (hash !== err[0].dtPassword) {
            throw new HttpException('Invalid Credentials', HttpStatus.UNAUTHORIZED);
        }
        const user: object = {
            'email': err[0].dtEmail,
            'id': err[0].idUser,
            'role': err[0].dtRole,
            'name': err[0].dtName
        };
        connection.end();
        return user;
    }


    public async getUserProfile(id: number, token: any){
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Controls if the person trying to update this profile is an admin or the user himself
        if (token.role !== 'admin'){
            if ( id.toString() !== token.id.toString()){
                //returns an error to the user
                throw new HttpException('You are not allowed to get this information!', HttpStatus.UNAUTHORIZED);
            }
        }

        const connection =  await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        //check if email exists
        const [err, result, fields] = await connection.execute('SELECT * from tblUser where idUser =?',[id]);

        if (err.length !== 1){
            throw new HttpException('No User Found', HttpStatus.UNAUTHORIZED);
        }

        const user: object = {
            'id': err[0].idUser,
            'name': err[0].dtName,
            'birthdate': err[0].dtBirthdate,
            'email': err[0].dtEmail,
            'role': err[0].dtRole,

        };
        connection.end();
        return user;
    }




    public async getAllUserProfile(token: any){
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);


        //Controls if the person trying to get all the profiles is an admin
        if (token.role !== 'admin'){
            //returns an error to the user
            throw new HttpException('You are not allowed to get this information!', HttpStatus.UNAUTHORIZED);

        }

        const connection =  await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        const [err, result, fields] = await connection.execute('SELECT * from tblUser');


        if (err.length < 1){
            throw new HttpException('No User Found', HttpStatus.UNAUTHORIZED);
        }

        let users: any = [];

        for (let i = 0; i < err.length; i++){
            const user: object = {
                'id': err[i].idUser,
                'name': err[i].dtName,
                'birthdate': err[i].dtBirthdate,
                'email': err[i].dtEmail,
                'role': err[i].dtRole,

            };
            users.push(user);
        }

        connection.end();
        return JSON.stringify(users);
    }


    public async updateProfile(user: UserDto, token: any, idOfUser: number) {
        //Slices the token (in the beginning it written Bearer with a space)
        token = token.slice(7);

        //Decodes the token an gets an object
        token = JWT(token);

        //Initializes the variables to connect to the db
        const connection = await this.mysql.createConnection({
            host: dbInformation.host,
            user: dbInformation.user,
            database: dbInformation.database,
            password: dbInformation.password
        });

        let {id,
            Name,
            BirthDate,
            Email,
            Password,
            Salt, Role} = user;
        console.log(id,
            Name,
            BirthDate,
            Email,
            Password,
            Salt, Role);

        //Controls if the person trying to update this profile is an admin or the user himself
        if (token.role !== 'admin'){

            if ( idOfUser.toString() !== token.id.toString()){
                //returns an error to the user
                throw new HttpException('You are not allowed to edit this user!', HttpStatus.UNAUTHORIZED);
            }
        }

        if (token.role === 'admin'){
            //Controls if password is defined or not
            if (Password === undefined){
                try {
                    console.log('here');
                    console.log(Name, BirthDate, Email, id, Role);
                    //Upadates the user profile
                    await connection.execute(
                        'UPDATE tblUser SET dtName= ?, dtBirthdate=?, dtEmail=?, dtRole=? WHERE (idUser=?);',
                        [Name, BirthDate, Email, Role, id ]
                    );

                    //Closes db connections
                    connection.end();

                    //return a success message to user
                    return JSON.stringify({ done: true, message : 'User Profile successfully updated!' });
                } catch (e){
                    console.log(e);
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }else {
                try {
                    //create a random salt and then encrypt the password
                    const salt: string = crypto.randomBytes(32).toString('hex');
                    const encryptPassword: string = crypto.pbkdf2Sync(Password, salt, 10000, 100, 'sha512').toString('hex');
                    Salt = salt;
                    Password = encryptPassword;
                    //Upadates the user profile
                    await connection.execute(
                        'UPDATE tblUser SET dtName= ?, dtBirthdate=?, dtEmail=?, dtPassword=?, dtSalt=?, dtRole=? WHERE (idUser=?);',
                        [Name, BirthDate, Email, Password, Salt, Role, id]
                    );

                    //Closes db connections
                    connection.end();

                    //return a success message to user
                    return JSON.stringify({ done: true, message : 'User Profile successfully updated!' });
                } catch{
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }



        }else {
            //Controls if password is defined or not
            if (Password === undefined){
                try {
                    //Upadates the user profile
                    await connection.execute(
                        'UPDATE tblUser SET dtName= ?, dtBirthdate=?, dtEmail=? WHERE (idUser=?);',
                        [Name, BirthDate, Email, id]
                    );

                    //Closes db connections
                    connection.end();

                    //return a success message to user
                    return JSON.stringify({ done: true, message : 'User Profile successfully updated!' });
                } catch{
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }else {
                try {
                    //create a random salt and then encrypt the password
                    const salt: string = crypto.randomBytes(32).toString('hex');
                    const encryptPassword: string = crypto.pbkdf2Sync(Password, salt, 10000, 100, 'sha512').toString('hex');
                    Salt = salt;
                    Password = encryptPassword;
                    //Upadates the user profile
                    await connection.execute(
                        'UPDATE tblUser SET dtName= ?, dtBirthdate=?, dtEmail=?, dtPassword=?, dtSalt=? WHERE (idUser=?);',
                        [Name, BirthDate, Email, Password, Salt, id]
                    );

                    //Closes db connections
                    connection.end();

                    //return a success message to user
                    return JSON.stringify({ done: true, message : 'User Profile successfully updated!' });
                } catch{
                    //returns an error to the user
                    throw new HttpException('Internal error occurred', HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
        }




    }
}
