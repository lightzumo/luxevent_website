import { Module } from '@nestjs/common';
import { AngularUniversalModule } from '@nestjs/ng-universal';
import { join } from 'path';
import { AppServerModule } from '../src/main.server';
import { AppController } from './app.controller';
import { AuthController } from './src/auth/auth.controller';
import { AuthService } from './src/auth/auth.service';
import { AuthModule } from './src/auth/auth.module';
import { UserModule } from './src/user/user.module';
import { EventModule } from './src/event/event.module';
import { GenreController } from './src/genre/genre.controller';
import { GenreService } from './src/genre/genre.service';

@Module({
  imports: [
    AngularUniversalModule.forRoot({
      bootstrap: AppServerModule,
      viewsPath: join(process.cwd(), 'dist/LuxEvent/browser')
    }),
    AuthModule,
    UserModule,
    EventModule
  ],
  controllers: [AppController, AuthController, GenreController],
  providers: [GenreService]
})
export class ApplicationModule {}
