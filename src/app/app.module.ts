import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { AppComponent } from './app.component';
import { SharedModule } from './modules/shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieService } from 'ngx-cookie-service';
import {MatSliderModule} from "@angular/material/slider";
import { HomeComponent } from './components/home/home.component';
import {MaterialModule} from "./modules/material/material.module";
import { MainComponent } from './components/main/main.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatToolbarModule} from "@angular/material/toolbar";
import {MatIconModule} from "@angular/material/icon";
import {MatMenuModule} from "@angular/material/menu";
import {MatListModule} from "@angular/material/list";
import {MAT_DATE_LOCALE} from "@angular/material/core";
import { RegisterFormComponent } from './components/register-form/register-form.component';
import {MatCardModule} from "@angular/material/card";
import {MAT_FORM_FIELD_DEFAULT_OPTIONS, MatFormFieldModule} from "@angular/material/form-field";
import {MatRadioModule} from "@angular/material/radio";
import {ReactiveFormsModule} from "@angular/forms";
import { LoginFormComponent } from './components/login-form/login-form.component';
import { CreateEventComponent } from './components/create-event/create-event.component';
import { CookieModule } from 'ngx-cookie';
import { EventDetailsComponent, DialogSaveEvent } from './components/event-details/event-details.component';
import { MatAutocompleteModule}  from "@angular/material/autocomplete";
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import {MatSelectModule} from "@angular/material/select";
import {LogoutComponent} from "./components/logout/logout.component";
import {JwtInterceptor} from "./services/jwt/jwt.interceptor";
import {CreateGenreComponent} from "./components/create-genre/create-genre.component";
import { MyEventsComponent } from './components/my-events/my-events.component';
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatSortModule} from "@angular/material/sort";
import {MatProgressSpinnerModule} from "@angular/material/progress-spinner";
import {EventListComponent} from "./components/event-list/event-list.component";

import {MapService} from "./services/map.service";
import {MatDialogModule} from "@angular/material/dialog";
import { SearchEventFormComponent } from './components/search-event-form/search-event-form.component';
import {EditEventComponent} from "./components/edit-event/edit-event.component";
import {EventsAttendingComponent} from "./components/events-attending/events-attending.component";
import {ShowUserEventsComponent} from "./components/show-user-events/show-user-events.component";
import {AllEventListComponent} from "./components/all-event-list/all-event-list.component";
import {UserListComponent} from "./components/user-list/user-list.component";
import {UserUpdateComponent} from "./components/user-update/user-update.component";


@NgModule({
  declarations: [AppComponent, HomeComponent, MainComponent, RegisterFormComponent, LoginFormComponent, CreateEventComponent,
                 EventDetailsComponent, LogoutComponent, CreateGenreComponent, MyEventsComponent, DialogSaveEvent,
                 SearchEventFormComponent, EventListComponent, EditEventComponent, EventsAttendingComponent, ShowUserEventsComponent,
                 AllEventListComponent, UserListComponent, UserUpdateComponent],
  imports: [
    HttpClientModule,
    BrowserModule.withServerTransition({appId: 'my-app'}),
    TransferHttpCacheModule,
    CookieModule.forRoot(),
    NgxMaterialTimepickerModule.setLocale('lb-LU'),

    RouterModule.forRoot([
      {
        path: '',
        component: HomeComponent,
        pathMatch: 'full'
      },
      {
        path: 'register',
        component: RegisterFormComponent,
      },
      {
        path: 'login',
        component: LoginFormComponent,
      },
      {
        path: 'create/event',
        component: CreateEventComponent,
      },
      {
        path: 'edit/event/:id',
        component: EditEventComponent,
      },
      {
        path: 'event/:id',
        component: EventDetailsComponent,
      },
      {
        path: 'my/events',
        component: ShowUserEventsComponent,
      },
      {
        path: 'logout',
        component: LogoutComponent,
      },
      {
        path: 'create/genre',
        component: CreateGenreComponent,
      },
      {
        path: 'events/admin',
        component: AllEventListComponent,
      },
      {
        path: 'user/admin',
        component: UserListComponent,
      },
      {
        path: 'update/user/:id',
        component: UserUpdateComponent,
      },
      {
        path: '**',
        component: HomeComponent,
      },
    ]),
    SharedModule,
    BrowserAnimationsModule,
    MaterialModule,
    MatAutocompleteModule,
    MatSelectModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatDialogModule,

  ],
  entryComponents: [DialogSaveEvent, EventDetailsComponent],

  providers: [
    CookieService,
    MapService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: MAT_DATE_LOCALE, useValue: 'en-GB' },
    { provide: MAT_FORM_FIELD_DEFAULT_OPTIONS, useValue: { appearance: 'fill' } },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
