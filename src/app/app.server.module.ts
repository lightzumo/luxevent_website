import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import {
  ServerModule,
  ServerTransferStateModule
} from "@angular/platform-server";
import { AppComponent } from "./app.component";
import { AppModule } from "./app.module";
import { UniversalInterceptorService } from "./modules/shared/interceptors/universal-interceptor.service";
import {MatSliderModule} from "@angular/material/slider";
import { CookieService, CookieBackendService } from 'ngx-cookie';

@NgModule({
  imports: [
    // The AppServerModule should import your AppModule followed
    // by the ServerModule from @angular/platform-server.
    AppModule,
    ServerModule,
    ServerTransferStateModule,
    MatSliderModule,
  ],
  // Since the bootstrapped component is not inherited from your
  // imported AppModule, it needs to be repeated here.
  bootstrap: [AppComponent],
  providers: [{ provide: CookieService, useClass: CookieBackendService }],
})
export class AppServerModule {}
