import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {EventService} from "../../services/event/event.service";
import {Subscription} from "rxjs";

//Event DTO
export interface EventData {
  id: number;
  title: string;
  startDate: any;
  endDate: any;
}


@Component({
  selector: 'app-my-events',
  templateUrl: './my-events.component.html',
  styleUrls: ['./my-events.component.scss']
})
export class MyEventsComponent implements OnInit {
    //Columns that will be displayed
  displayedColumns: string[] = ['id', 'name', 'startDate', 'endDate', 'edit', 'view'];
  dataSource: MatTableDataSource<EventData>;

  private sub: Subscription;

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    if (typeof(window) !== 'undefined'){
        //fetches the events that the current user logged in created and displays it
      this.sub = this.eventService.getAllUserEvents().subscribe(
          data => {

            const events: EventData[] = <EventData[]> data;
            const options = {month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit'};
            for (let i = 0; i < events.length; i++) {
              const formattedStartDate = new Date(events[i].startDate);
              events[i].startDate = formattedStartDate.toLocaleString("en-GB", options);
              const formattedEndDate = new Date(events[i].endDate);
              events[i].endDate = formattedEndDate.toLocaleString("en-GB", options);
            }


            this.dataSource = new MatTableDataSource<EventData>(events);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          error => console.error(error)
      );

    }

  }

  //Filters the events written by the user
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
