import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {UserService} from "../../services/user/user.service";
import {JwtService} from "../../services/jwt/jwt.service";
import {MatTableDataSource} from "@angular/material/table";
import {UserData} from "../user-list/user-list.component";
import * as JWT from 'jwt-decode';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: ['./user-update.component.scss']
})
export class UserUpdateComponent implements OnInit {

  // data that will be sent to be updated
  public data: { [k: string]: any } = {};
  //Form Control
  public registerCred = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', ),
    birthday: new FormControl('', Validators.required),
    role: new FormControl('user', Validators.required)
  });
  selected = 'user';

  role = '';


  //subscription used to get the data
  private sub: Subscription;

  //Error Message
  public errorMessage: string;

  public token: any;

  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService, private jwtService: JwtService,) { }

  ngOnInit(): void {

    if (typeof(window) !== 'undefined'){
      //If user is not authenticated user cannot access the page
      if (!this.jwtService.isAuthenticated()){
        this.router.navigateByUrl('').then();
      }


      this.token = JWT(this.jwtService.getAuthToken());
      //If user is not an admin leave page
      if (this.token.role !== 'admin'){
        if (this.token.id !== this.getIdInURL()){
          this.router.navigateByUrl('').then();
        }

      }
      this.role = this.token.role;



      //Gets Events
      this.sub = this.userService.getUser(this.getIdInURL()).subscribe(
          data => {
            console.log('this is data', data);
            //Set Event data into form
            this.registerCred.controls['name'].setValue(data['name']);
            this.registerCred.controls['email'].setValue(data['email']);
            this.registerCred.controls['birthday'].setValue(data['birthdate']);
            this.registerCred.controls['role'].setValue(data['role']);
          },
          error => console.error('this is error', error)
      );
    }

  }


  //gets the id of the event the user is trying to edit
  private getIdInURL(): number {
    return +this.route.snapshot.paramMap.get('id');
  }


  //Update Profile
  public update(): void {

    if (this.registerCred.valid) {

      const formattedBirthday = new Date(this.registerCred.get('birthday').value);

      const newFormattedDate = String(formattedBirthday.getFullYear() + "-" + (formattedBirthday.getMonth()+1) + "-" + formattedBirthday.getDate());
      this.data.Name = this.registerCred.get('name').value;
      this.data.Email = this.registerCred.get('email').value;

      if (this.registerCred.get('password').value){
        this.data.Password = this.registerCred.get('password').value;
      }

      this.data.Role = this.registerCred.get('role').value;
      this.data.BirthDate = formattedBirthday;
      this.data.id = this.getIdInURL();

      this.errorMessage = "Updating User...";
      this.sub = this.userService.updateUser(this.getIdInURL(),this.data).subscribe(
          data => this.errorMessage = data['message'],
          error => this.errorMessage = "Error updating user"
      );
    } else {
      this.errorMessage = '';
    }
  }

}
