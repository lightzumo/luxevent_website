import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {GenreService} from "../../services/genre/genre.service";
import {Subscription} from "rxjs";
import {JwtService} from "../../services/jwt/jwt.service";
import {Router} from "@angular/router";
import * as JWT from 'jwt-decode';

@Component({
  selector: 'app-create-genre',
  templateUrl: './create-genre.component.html',
  styleUrls: ['./create-genre.component.scss']
})
export class CreateGenreComponent implements OnInit {

  private sub: Subscription;

  public errorMessage: string;

  //Form control for the creation  of the genre
  public createGenre: FormGroup = new FormGroup({
    description: new FormControl('', [Validators.required, Validators.maxLength(40)]),
  });

  constructor(private genreService: GenreService, private jwtService: JwtService, private router : Router) { }

  ngOnInit(): void {
    //If user is not authenticated user cannot access the page
    if (typeof(window) !== 'undefined'){
      if (!this.jwtService.isAuthenticated()){
        this.router.navigateByUrl('').then();
      }

      const token = JWT(this.jwtService.getAuthToken());

      //If user is not an admin he cannot access this page
      if (token.role !== 'admin'){
        this.router.navigateByUrl('').then();
      }
    }
  }

  //shows the response of the server
  private setMessage(message: any): void {
    this.errorMessage = message.message;
  }


  //Creates the genre by sending the data to the service that sends it to the server and awaits a response
  public create() {
    if (this.createGenre.valid){
      this.sub = this.genreService.createGenre(this.createGenre.value).subscribe(
          data => this.setMessage(data),
          error => this.setMessage(error)
      )
    }
  }


/*  ngOnDestroy(){
   this.sub.unsubscribe();
  }*/
}
