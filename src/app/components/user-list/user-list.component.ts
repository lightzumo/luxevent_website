import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {EventData} from "../my-events/my-events.component";
import {Subscription} from "rxjs";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {EventService} from "../../services/event/event.service";
import {JwtService} from "../../services/jwt/jwt.service";
import {Router} from "@angular/router";
import * as JWT from 'jwt-decode';
import {UserService} from "../../services/user/user.service";
//Event DTO
export interface UserData {
  id: number;
  name: string;
  email: any;
  birthdate: any;
}

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})


export class UserListComponent implements OnInit {

  //Columns that will be displayed
  displayedColumns: string[] = ['id', 'name', 'email', 'edit'];

  //What will be displayed in the table
  dataSource: MatTableDataSource<UserData>;

  private sub: Subscription;

  //Childs that will change the page (Show more events/Sorts Events)
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private userService: UserService, private jwtService: JwtService, private router : Router) { }

  ngOnInit() {
    //Waits for the page to be loaded
    if (typeof(window) !== 'undefined'){

      //If user is not authenticated user cannot access the page
      if (!this.jwtService.isAuthenticated()){
        this.router.navigateByUrl('').then();
      }

      const token = JWT(this.jwtService.getAuthToken());
      //If user is not an admin leave page
      if (token.role !== 'admin'){
        this.router.navigateByUrl('').then();
      }

      //Gets Events
      this.sub = this.userService.getAllUsers().subscribe(
          data => {
            const users: UserData[] = <UserData[]> data;

            this.dataSource = new MatTableDataSource<UserData>(users);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          error => console.error(error)
      );

    }

  }

  //Filters the Events when user types in
  public applyFilter(user: Event) {
    const filterValue = (user.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
