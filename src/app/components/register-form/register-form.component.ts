import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import {AuthService} from "../../services/auth/auth.service";
import {Router} from "@angular/router";
import {Subscription} from "rxjs";
import {JwtService} from "../../services/jwt/jwt.service";
@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {
  // data that will be sent to be updated
  public data: { [k: string]: any } = {};
  //Form Control
  public registerCred = new FormGroup({
    name: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', [Validators.required, Validators.minLength(8)]),
    birthday: new FormControl('', Validators.required)
  });


  //subscription used to get the data
  private sub: Subscription;

  //Error Message
  public errorMessage: string;

  constructor(private authService: AuthService, private router: Router, private jwtService: JwtService) { }

  ngOnInit(): void {
    //If user is not authenticated user cannot access the page
    if (this.jwtService.isAuthenticated()){
      this.router.navigateByUrl('').then();
    }
  }

  private registrationSuccess(data): void {
    if (data.done === true){
      this.router.navigateByUrl('/login').then();
    }else {
      this.errorMessage = data.message;
    }
  }

  private registrationError(error: any): void {
    console.log(error);
    this.errorMessage = 'An Error occurred!';
  }

  //Register Form
  public register(): void {
    if (this.registerCred.valid) {

      const formattedBirthday = this.registerCred.get('birthday').value;
      const newFormattedDate = String(formattedBirthday.getFullYear() + "-" + (formattedBirthday.getMonth()+1) + "-" + formattedBirthday.getDate());
      this.data.name = this.registerCred.get('name').value;
      this.data.email = this.registerCred.get('email').value;
      this.data.password = this.registerCred.get('password').value;
      this.data.birthday = formattedBirthday;

      this.sub = this.authService.register(this.registerCred).subscribe(
         data => this.registrationSuccess(data),
          error => this.registrationError(error)
      );
    } else {
      this.errorMessage = 'Please fill all the required fields and then Register again.';
    }
  }


/*  ngOnDestroy() {
    //Destroys every subscription to free up RAM
    this.sub.unsubscribe();
  }*/

}
