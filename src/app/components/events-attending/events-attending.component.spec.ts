import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EventsAttendingComponent } from './events-attending.component';

describe('EventsAttendingComponent', () => {
  let component: EventsAttendingComponent;
  let fixture: ComponentFixture<EventsAttendingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventsAttendingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsAttendingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
