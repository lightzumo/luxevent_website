import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Subscription} from "rxjs";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {EventService} from "../../services/event/event.service";
import {EventData} from "../my-events/my-events.component";

@Component({
  selector: 'app-events-attending',
  templateUrl: './events-attending.component.html',
  styleUrls: ['./events-attending.component.scss']
})
export class EventsAttendingComponent implements OnInit {

  //Columns that will be displayed and the data
  displayedColumns: string[] = ['id', 'name', 'startDate', 'endDate', 'view'];
  dataSource: MatTableDataSource<EventData>;


  private sub: Subscription;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private eventService: EventService) { }

  ngOnInit() {
    if (typeof(window) !== 'undefined'){

        //Fetches all events the current user will be attending
      this.sub = this.eventService.getAllUserAttendEvents().subscribe(
          data => {
              //Add the events fetched to the table
            const events: EventData[] = <EventData[]> data;
            const options = {month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit'};
            for (let i = 0; i < events.length; i++) {
              const formattedStartDate = new Date(events[i].startDate);
              events[i].startDate = formattedStartDate.toLocaleString("en-GB", options);
              const formattedEndDate = new Date(events[i].endDate);
              events[i].endDate = formattedEndDate.toLocaleString("en-GB", options);
            }


            this.dataSource = new MatTableDataSource<EventData>(events);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          error => console.error(error)
      );

    }

  }

  // Searches the events when user types into the searchbox
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
