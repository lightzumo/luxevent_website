import {Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource} from "@angular/material/table";
import {Subscription} from "rxjs";
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {EventService} from "../../services/event/event.service";
import {EventData} from "../my-events/my-events.component";
import * as JWT from 'jwt-decode';
import {JwtService} from "../../services/jwt/jwt.service";
import {Router} from "@angular/router";
@Component({
  selector: 'app-all-event-list',
  templateUrl: './all-event-list.component.html',
  styleUrls: ['./all-event-list.component.scss']
})
export class AllEventListComponent implements OnInit {

  //Columns that will be displayed
  displayedColumns: string[] = ['id', 'name', 'startDate', 'endDate', 'edit', 'view'];

  //What will be displayed in the table
  dataSource: MatTableDataSource<EventData>;

  private sub: Subscription;

  //Childs that will change the page (Show more events/Sorts Events)
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  constructor(private eventService: EventService, private jwtService: JwtService, private router : Router) { }

  ngOnInit() {
      //Waits for the page to be loaded
    if (typeof(window) !== 'undefined'){

        const token = JWT(this.jwtService.getAuthToken());
        //If user is not an admin leave page
        if (token.role !== 'admin'){
            this.router.navigateByUrl('').then();
        }

        //Gets Events
      this.sub = this.eventService.getAllEvents().subscribe(
          data => {

            const events: EventData[] = <EventData[]> data;
            const options = {month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit'};
            for (let i = 0; i < events.length; i++) {
              const formattedStartDate = new Date(events[i].startDate);
              events[i].startDate = formattedStartDate.toLocaleString("en-GB", options);
              const formattedEndDate = new Date(events[i].endDate);
              events[i].endDate = formattedEndDate.toLocaleString("en-GB", options);
            }

            this.dataSource = new MatTableDataSource<EventData>(events);
            this.dataSource.paginator = this.paginator;
            this.dataSource.sort = this.sort;
          },
          error => console.error(error)
      );

    }

  }

  //Filters the Events when user types in
  public applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
