import {Component, ElementRef, NgZone, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MapboxService} from "../../services/mapbox/mapbox.service";
import {GenreService} from "../../services/genre/genre.service";
import {EventService} from "../../services/event/event.service"
import {Subscription} from "rxjs";
import {JwtService} from "../../services/jwt/jwt.service";
import {Router} from "@angular/router";

interface Genre {
  GenreId: number;
  GenreName: string;
}
interface MapBoxAPI {
  type: string;
  query: string[];
  features: any[];
  attribution: string;
}

@Component({
  selector: 'app-create-event',
  templateUrl: './create-event.component.html',
  styleUrls: ['./create-event.component.scss']
})
export class CreateEventComponent implements OnInit {

  //min date to create an event is today
  public minDate: Date = new Date();

  //subscription used to get the data
  private sub: Subscription;

  //saves all genres that was returned by the API and displays it on the select box
  public genres: Genre[];

  //selects the address the user clicked on
  public selectedAddress = null;

  //saves the addresses that API returned and display on the autocomplete
  public addresses: MapBoxAPI;

  //message that will be displayed after creating the event-details (can be an error or an success message!)
  public errorMessage: string = '';
  //Selects the file to upload
  public fileToUpload: File = null;

  //Message that is displayed on the button to upload an image
  fileMessage: string = 'Upload Image!';

  //FormControl of event-details form
  public createEvent: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.maxLength(55)]),
    description: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    address: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    startTime: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    endTime: new FormControl('', [Validators.required]),
    genre: new FormControl('', [Validators.required]),
    image: new FormControl(''),
    price: new FormControl(null, [Validators.min(0), Validators.pattern("[0-9]+(?:\\.[0-9]+)?"),])
    //pattern found on: https://stackoverflow.com/questions/47030843/regex-pattern-for-period-separator-and-no-comma-angular-2
  });

  constructor(private  mapboxService: MapboxService, private genreService: GenreService, private eventService: EventService,
              private  jwtService: JwtService, private router: Router) { }

  ngOnInit(): void {

    if (typeof(window) !== 'undefined'){
      //If user is not authenticated user cannot access the page
      if (!this.jwtService.isAuthenticated()){
        this.router.navigateByUrl('').then();
      }

      //gets all genres of event-details and displays it
      this.sub = this.genreService.getAllGenres().subscribe(
          data => this.genres = <Genre[]> data['category'],
          error => this.errorMessage
      );
    }
  }

  //show a message when an error happend
  private setMessage(error: any): void {
    this.errorMessage = error.message;
  }

  //Creates the event-details
  public create(): void {
    if (this.createEvent.valid) {
      // Gets both end and start time of the event-details
      const startTime = this.createEvent.get('startTime').value;
      const endTime = this.createEvent.get('endTime').value;

      //this.successMessage = '';
      this.errorMessage = '';

      // converts the time into hours and minutes
      const startTimeHours = parseInt(startTime.slice(0, 2), 10);
      const startTimeMinutes = parseInt(startTime.slice(3, 5), 10);
      const endTimeHours = parseInt(endTime.slice(0, 2), 10);
      const endTimeMinutes = parseInt(endTime.slice(3, 5), 10);

      // creates the date of the start and end of the event-details with the time
      const endDate = new Date(this.createEvent.get('endDate').value)
      endDate.setHours(endTimeHours, endTimeMinutes, 0, 0);
      const startDate = new Date(this.createEvent.get('startDate').value);
      startDate.setHours(
          startTimeHours,
          startTimeMinutes,
          0,
          0,
      );

      //Control if the dates are correct
      if (endDate <= startDate) {
        this.errorMessage = 'Check your dates!';
        return;
      }

      //Formats the dates to send to the server
      const formatted_startDate =
          startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() +  ' '
          + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds();

      const formatted_endDate =
          endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() +  ' '
          + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds();

      //Object that will be sent to the server
      let data: { [k: string]: any } = {};

      const form = new FormData();
      form.append("title", this.createEvent.get('title').value);
      form.append("startDate", formatted_startDate);
      form.append("endDate", formatted_endDate);
      if (this.createEvent.get('price').value !== ''){
        form.append("price", this.createEvent.get('price').value);
      }
      form.append("address", this.createEvent.get('address').value);
      form.append("description",  this.createEvent.get('description').value);
      form.append("longitude", this.selectedAddress[0]);
      form.append("latitude", this.selectedAddress[1]);
      if (this.fileToUpload !== null){
        form.append("image", this.fileToUpload, this.fileToUpload.name);
      }
      form.append("category", this.createEvent.get('genre').value);

    this.errorMessage = "Creating...";

      // sends data
      this.sub = this.eventService.createEvent(form).subscribe(
          data => this.setMessage(data),
          error => this.errorMessage = 'Error occured while creating your event-details, please try again later.',
      );
    }
  }


  //Function that search for the address when user types a word
  public search(event: any): void{
    const searchTerm = event.toLowerCase();
    if (searchTerm && searchTerm.length > 0){
      this.sub = this.mapboxService.search_word(searchTerm).subscribe(
          data => this.addresses = <MapBoxAPI> data,
          error => this.errorMessage = 'Error loading addresses, please try again later.',
      );
      //this.addresses = this.addresses['features'];
    }
  }

  //Saved the address that the user selected
  public onSelect(address: any): void{
    // (0) long: 6.27737 , (1) lat: 49.503963
    this.selectedAddress = address.geometry.coordinates;
  }


  //Gets the file selected
  public onFileSelected(event: any):void {
    this.fileToUpload = event.target.files[0];
    this.fileMessage = this.fileToUpload.name
  }


  ngOnDestroy() {
    //Destroys every subscription to free up RAM
    this.sub.unsubscribe();
  }
}
