import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUserEventsComponent } from './show-user-events.component';

describe('ShowUserEventsComponent', () => {
  let component: ShowUserEventsComponent;
  let fixture: ComponentFixture<ShowUserEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowUserEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUserEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
