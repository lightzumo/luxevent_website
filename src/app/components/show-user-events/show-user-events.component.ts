import { Component, OnInit } from '@angular/core';
//little menu that switchs beetwen showing the user his events (created ones) and the events he is attending
@Component({
  selector: 'app-show-user-events',
  templateUrl: './show-user-events.component.html',
  styleUrls: ['./show-user-events.component.scss']
})
export class ShowUserEventsComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  public showMyEvents:boolean = true;

  public showEvent(): void{
    this.showMyEvents = true;
  }

  public showEventAttend(): void{
    this.showMyEvents = false;
  }


}
