import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/internal/operators';
import { Router } from '@angular/router';
import {CookieService} from "ngx-cookie";
import {JwtHelper} from "../jwtHelper";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
//This component displays the Nav bar on the top of the screen
export class MainComponent implements OnInit {
  isHandset$: Observable<boolean> = this._breakpointObserver.observe(Breakpoints.Handset).pipe(
      map(result => result.matches),
      shareReplay(),
  );

  constructor(
      private _breakpointObserver: BreakpointObserver,
      private _router: Router,
      private _cookieService:CookieService
  ) {}

  public navItems: object;
  public dropDowns: object;

  private initNav(token : any) {
    //Shows a different menu depending on the user role
    if (token.role == 'user') {
      this.navItems = [
        {
          name: 'Home',
          path: '/',
          icon: 'home',
        },
      ];
      this.dropDowns = [
        {
          name: token.name,
          icon: 'perm_identity',
          items: [
            {
              name: 'Create event',
              path: '/create/event',
              icon: 'event',
            },
            {
              name: 'My Events',
              path: '/my/events',
              icon: 'event',
            },
            {
              name: 'Update My Profile',
              path: '/update/user/'+ token.id,
              icon: 'create',
            },
            {
              name: 'Logout',
              path: '/logout',
              icon: 'power_settings_new',
            },
          ],
        },
      ];
    }else if (token.role === 'admin') {
      this.navItems = [
        {
          name: 'Home',
          path: '/',
          icon: 'home',
        },
      ];
      this.dropDowns = [
        {
          name: token.name,
          icon: 'perm_identity',
          items: [
            {
              name: 'Create event',
              path: '/create/event',
              icon: 'event',
            },
            {
              name: 'My Events',
              path: '/my/events',
              icon: 'event',
            },
            {
              name: 'Create genre',
              path: '/create/genre',
              icon: 'more',
            },
            {
              name: 'Edit Events',
              path: '/events/admin',
              icon: 'create',
            },
            {
              name: 'Edit Users',
              path: '/user/admin',
              icon: 'create',
            },
            {
              name: 'Update My Profile',
              path: '/update/user/'+ token.id,
              icon: 'create',
            },
            {
              name: 'Logout',
              path: '/logout',
              icon: 'power_settings_new',
            },
          ],
        },
      ];
    } else {
      this.navItems = [
        {
          name: 'Home',
          path: '/',
          icon: 'home',
        },
        {
          name: 'Login',
          path: '/login',
          icon: 'person',
        },
        {
          name: 'Register',
          path: '/register',
          icon: 'person_add',
        },
      ];
      this.dropDowns = [];
    }
  }

  ngOnInit() {
    //Decodes the token and initializes the nav bar
    const token = JwtHelper.decodeToken(this._cookieService.get('LuxEvent'));
    this.initNav(token);
  }
}
