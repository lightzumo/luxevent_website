import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {GenreService} from "../../services/genre/genre.service";
import {Subscription} from "rxjs";

interface Genre {
  GenreId: number;
  GenreName: string;
}

@Component({
  selector: 'app-search-event-form',
  templateUrl: './search-event-form.component.html',
  styleUrls: ['./search-event-form.component.scss']
})
export class SearchEventFormComponent implements OnInit {


  public minDate: Date = new Date();

  //subscription used to get the data
  private sub: Subscription;

  //saves all genres that was returned by the API and displays it on the select box
  public genres: Genre[];

  public searchData;
  @Output() messageSearchData = new EventEmitter<any>();

  //Form Control
  public search = new FormGroup({
    date: new FormControl('', Validators.required),
    genre: new FormControl('', [Validators.required]),

  });

  constructor(private genreService: GenreService) { }



  ngOnInit(): void {
    //gets all genres of event-details and displays it
    if (typeof(window) !== 'undefined'){
      this.sub = this.genreService.getAllGenres().subscribe(
          data => this.genres = <Genre[]> data['category'],
          error => console.log('this is error', error)
      );
    }
  }


  public searchEvent(): void{
    if(this.search.valid){
      this.searchData = {
          date: this.search.get('date').value,
          genre: this.search.get('genre').value
      };
      this.messageSearchData.emit(this.searchData);
    }
}

}
