import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/auth/auth.service";
import {CookieService} from "ngx-cookie";
import {JwtService} from "../../services/jwt/jwt.service";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  //either displays the password or not
  public  hidePassword = true;

  public errorMessage: string;

  public loginCred = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
  });

  constructor(private authService: AuthService, private router: Router, private _cookieService:CookieService,
              private jwtService: JwtService ) {}

  ngOnInit() {
    //If user is not authenticated user cannot access the page
    if (this.jwtService.isAuthenticated()){
      this.router.navigateByUrl('').then();
    }
  }

  private loginSuccess(token: any): void {

   const date = new Date();
    // Set it expire in 5 days
    date.setTime(date.getTime() + 7 * 24 * 60 * 60 * 1000);
    this._cookieService.put('LuxEvent',token.access_token, {expires : date});
    this.errorMessage = '';
    this.router.navigateByUrl('/').then();
  }

  private loginError(error: any): void {
    this.errorMessage = error.error.message;
  }

  //Logs in when user clicks on the button login
  public login(): void {
    if (this.loginCred.valid) {
      this.authService.login(this.loginCred.get('email').value, this.loginCred.get('password').value).subscribe(
          data => this.loginSuccess(data),
          error => this.loginError(error),
      );
    } else {
      this.errorMessage = 'Enter your email or/and password.';
    }
  }
}
