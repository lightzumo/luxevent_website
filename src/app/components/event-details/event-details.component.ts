import {Component, ElementRef, Inject, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {EventService} from "../../services/event/event.service";
import { Subscription} from "rxjs";


import { MapService } from '../../services/map.service';
import { Map, Marker, Icon } from 'leaflet';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from "@angular/material/dialog";
import {JwtService} from "../../services/jwt/jwt.service";

export interface eventInterface {
  id: number;
  title: string;
  startDate: Date;
  endDate: Date;
  price: number;
  address: string;
  description: string;
  longitude: number;
  latitude: number;
  image: string;
  category: number;
}

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})

export class EventDetailsComponent implements OnInit {

  private sub: Subscription;

  //Says if event was liked, be default it isn't
  public liked: boolean = false;

  //Event Object
  public event: eventInterface;

  //Dates that will be displayed in the screen
  public startDate;
  public endDate;

  //Map and marker that will be displayed
  private map: Map;
  private marker: Marker;


  constructor(private router: Router, private route: ActivatedRoute, private eventService: EventService, private mapService: MapService,
              private elementRef:ElementRef, public dialog: MatDialog,  public jwtService: JwtService) { }

  //When page is loaded this function will be called
  ngOnInit(): void {
    this.setData()
  }

  //Function that gets the information from the event in the Backend and creates also the map
  private setData(){
      if (this.mapService.L) {
          this.sub = this.eventService.getEvent(this.getIdInURL()).subscribe(
              data => {
                  this.event = <eventInterface>data;

                  //Options that will be used to write the date in the page
                  const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit'};
                  let newDate = new Date(this.event.endDate);
                  this.endDate = newDate.toLocaleString("en-GB", options);
                  newDate = new Date(this.event.startDate);
                  this.startDate = newDate.toLocaleString("en-GB", options);

                  if (this.jwtService.isAuthenticated()){
                      this.sub = this.eventService.doesUserLikeEvent(this.event.id).subscribe(
                          data => this.liked = <boolean> data['liked'],
                          error => console.log(error)
                      )
                  }

                  //creates the map
                  this.setupMap();
              },
              error => console.log(error)
          );

      }
  }

  //Function that creates the map
  private setupMap() {
    // Create the map in the #map container
    this.map = this.mapService.L.map('map').setView([this.event.latitude, this.event.longitude], 16);

    // Add a tilelayer
      this.mapService.L.tileLayer('https://{s}.tile.openstreetmap.de/tiles/osmde/{z}/{x}/{y}.png', {
          maxZoom: 18,
          attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
      }).addTo(this.map);

      //imports the icons that will be displayed on the map
     // const icon = require('leaflet/dist/images/marker-icon.png');
      const iconShadow = require('leaflet/dist/images/marker-shadow.png');
      const icon2x = require('leaflet/dist/images/marker-icon-2x.png');

      //assigns the icons
      this.mapService.L.icon({
        shadowUrl: iconShadow
      });

      //Adds a marken to the map where it shows the name of the event
     this.marker = this.mapService.L.marker([this.event.latitude,this.event.longitude]).addTo(this.map);
     this.marker.bindPopup(this.event.title).openPopup();

     //disables the user to drag and zoom the map
     this.map.dragging.disable();
     this.map.zoomControl.remove();
     this.map.scrollWheelZoom.disable();
  }

  //gets the id written in the url eg: event/45 it return 45
  private getIdInURL(): number {
    return +this.route.snapshot.paramMap.get('id');
  }

    //Function that will save the event to the users calendar
    public save(){
      //Opens an dialog so that the user can choose which calendar he/she uses
        const dialogRef = this.dialog.open(DialogSaveEvent, {
            width: '250px',
        });

        dialogRef.afterClosed().subscribe(result => {
            //If user clicked on cancel it does nothing
            if (result === null || result === undefined){
                return;
            }

            //If user clicked on iCalendar or Outlook an ICS file will be generated
            if (result !== 'google'){
                //creates the dates according to the ICS format
                const startDate =  this.formatDateForICSFile(this.event.startDate);
                const endDate = this.formatDateForICSFile(this.event.endDate);

                //creates the message that will be written in the ICS file
                const icsMSG = "BEGIN:VCALENDAR\nVERSION:2.0\nBEGIN:VEVENT\nDTSTART:" + startDate +
                    "\nDTEND:" + endDate +"\nSUMMARY:"+ this.event.title+
                    "\nDESCRIPTION:"+this.event.description+" The event-details is located in:"+ this.event.address +
                    "\nEND:VEVENT\nEND:VCALENDAR";


                //creates an a tag that will be automatically clicked on so that it downloads the file
                const element = document.createElement('a');
                element.setAttribute('href', 'data:text/calendar;charset=utf-8,' + encodeURIComponent(icsMSG));
                element.setAttribute('download', 'calendarTest.ics');
                element.style.display = 'none';
                document.body.appendChild(element);
                element.click();
            }

            //If user selected google calendar a tab will be open with the information already written on the form
            // and the user has to accept that it will be saved on his calendar
            if (result === 'google'){
                window.open('https://calendar.google.com/calendar/r/eventedit?text='+this.event.title +
                                '&dates='+this.formatDateForGoogleCalendar(this.event.startDate)
                                         +'/'+this.formatDateForGoogleCalendar(this.event.endDate)
                                +'&details='+this.event.description+" The event is located in:"+ this.event.address)
            }
        });
    }


    //Formats the Date so that it will be in the ics date standard
    private formatDateForICSFile(date): string{
        //Options that will be used to write the date in the page
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit'};
        const newDate = new Date(date);
        newDate.toLocaleString("en-GB", options);
        var pre =
            newDate.getFullYear().toString() +
            ((newDate.getMonth() + 1)<10? "0" + (newDate.getMonth() + 1).toString():( newDate.getMonth() + 1).toString()) +
            (( newDate.getDate() + 1)<10? "0" + newDate.getDate().toString():newDate.getDate().toString());

        var post = ((newDate.getHours())<10? "0" + (newDate.getHours()).toString():( newDate.getHours()).toString()) +
                   (( newDate.getMinutes())<10? "0" + ( newDate.getMinutes()).toString():(newDate.getMinutes()).toString()) + "00";

        return pre + "T" + post;

    }



    //Formats the Date so that it will be in the google event date standard
    private formatDateForGoogleCalendar(date){
        //Options that will be used to write the date in the page
        const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute:'2-digit'};
        const newDate = new Date(date);
        newDate.toLocaleString("en-GB", options);
        var pre =
            newDate.getFullYear().toString() +
            ((newDate.getMonth() + 1)<10? "0" + (newDate.getMonth() + 1).toString():( newDate.getMonth() + 1).toString()) +
            (( newDate.getDate() + 1)<10? "0" + newDate.getDate().toString():newDate.getDate().toString());

        var post =((newDate.getHours())<10? "0" + (newDate.getHours()).toString():( newDate.getHours()).toString()) +
            (( newDate.getMinutes())<10? "0" + ( newDate.getMinutes()).toString():(newDate.getMinutes()).toString()) + "00";

        return pre + "T" + post;
    }

    //Marks an event as going
    public going(): void{
      this.eventService.updateUserLikeEvent(this.event.id).subscribe(
          data => {
              if (data['done']){
                  if (this.liked){
                      this.liked = false;
                  }else {
                      this.liked = true;
                  }
              }
          }
      )
    }

}

//Creates an Dialog that will ass the user which kind of calendar he wants to save the event on
@Component({
    selector: 'dialog-save-event',
    templateUrl: 'dialog-save-event.html',
})
export class DialogSaveEvent {

    constructor(
        public dialogRef: MatDialogRef<DialogSaveEvent>, @Inject(MAT_DIALOG_DATA) public valueSelected: any) {}

        //If user clicks on cancel the selected value will be null so nothing will be downloaded
    public onNoClick(): void {
        this.valueSelected = null;
        this.dialogRef.close();
    }

}
