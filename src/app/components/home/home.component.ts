import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {SearchEventFormComponent} from "../search-event-form/search-event-form.component";
import {Subject} from "rxjs";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

//This component is only used to display the 2 components and send data between each other
export class HomeComponent implements OnInit {


  eventsSubject: Subject<void> = new Subject<void>();

  emitEventToChild(searchData) {
    this.eventsSubject.next(searchData);
  }

  constructor() { }

  ngOnInit(): void {
  }

  public receiveSearchData($event) {
    this.emitEventToChild($event);
  }

}
