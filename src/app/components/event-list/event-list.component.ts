import {Component, Input, OnInit} from '@angular/core';
import {EventService} from "../../services/event/event.service";
import {Observable, Subscription} from "rxjs";

@Component({
    selector: 'app-event-list',
    templateUrl: './event-list.component.html',
    styleUrls: ['./event-list.component.scss']
})
export class EventListComponent implements OnInit {

    constructor(private eventService: EventService) {
    }

    //--------Variables the will be used

    //Events array
    public events: any[];
    public noEvent: boolean = true;

    //Subscription
    private eventsSubscription: Subscription;

    //Watches if user searches for a specific type and date of event
    @Input() searchData: Observable<void>;


    ngOnInit(): void {
        const todayDate = new Date();
        //Formats the dates to send to the server
        const formatted_startDate =
            todayDate.getFullYear() + '-' + (todayDate.getMonth() + 1) + '-' + todayDate.getDate() + ' 00:00:00';


        if (typeof (window) !== 'undefined') {

            //Gets all events from today onwards
            this.eventService.searchEventsByDate(formatted_startDate).subscribe(
                data => this.setData(data),
                error => this.noEvent = true,
            );
            this.eventsSubscription = this.searchData.subscribe((data) => {

                const searchDate = new Date(data['date']);
                //Formats the dates to send to the server
                const formatted_searchDate =
                    searchDate.getFullYear() + '-' + (searchDate.getMonth() + 1) + '-' + searchDate.getDate() + ' 00:00:00';

                this.eventService.searchEvents(formatted_searchDate, data['genre']).subscribe(
                    data => this.setData(data),
                    error => this.noEvent = true,
                )
            });


        }
    }

    //Displays the event
    private setData(data: any): void {
        if (data === null) {
            this.noEvent = true;
        } else {
            this.noEvent = false;
            this.events = data;
        }
        const options = {weekday: 'short', month: 'short', day: 'numeric', hour: '2-digit', minute: '2-digit'};
        for (let i = 0; i < this.events.length; i++) {
            const formattedDate = new Date(this.events[i].startDate);
            this.events[i].startDate = formattedDate.toLocaleString("en-GB", options);

        }
    }

    /*  ngOnDestroy() {
        this.eventsSubscription.unsubscribe();
      }*/
}
