import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {MapboxService} from "../../services/mapbox/mapbox.service";
import {GenreService} from "../../services/genre/genre.service";
import {EventService} from "../../services/event/event.service";
import {JwtService} from "../../services/jwt/jwt.service";
import {ActivatedRoute, Router} from "@angular/router";
import * as JWT from 'jwt-decode';

interface Genre {
  GenreId: number;
  GenreName: string;
}
interface MapBoxAPI {
  type: string;
  query: string[];
  features: any[];
  attribution: string;
}


@Component({
  selector: 'app-edit-event',
  templateUrl: './edit-event.component.html',
  styleUrls: ['./edit-event.component.scss']
})
export class EditEventComponent implements OnInit {


  //subscription used to get the data
  private sub: Subscription;

  //saves all genres that was returned by the API and displays it on the select box
  public genres: Genre[];

  //selects the address the user clicked on
  public selectedAddress = null;

  //saves the addresses that API returned and display on the autocomplete
  public addresses: MapBoxAPI;

  //message that will be displayed after editing the event-details (can be an error or an success message!)
  public errorMessage: string = '';
  //Selects the file to upload
  public fileToUpload: File = null;

  //Message that is displayed on the button to upload an image
  public fileMessage: string = 'Upload new image!';

  public image;



  //FormControl of event-details form
  public editEvent: FormGroup = new FormGroup({
    title: new FormControl('', [Validators.required, Validators.maxLength(55)]),
    description: new FormControl('', [Validators.required, Validators.maxLength(255)]),
    address: new FormControl('', [Validators.required]),
    startDate: new FormControl('', [Validators.required]),
    startTime: new FormControl('', [Validators.required]),
    endDate: new FormControl('', [Validators.required]),
    endTime: new FormControl('', [Validators.required]),
    genre: new FormControl('', [Validators.required]),
    image: new FormControl(''),
    price: new FormControl(null, [Validators.min(0), Validators.pattern("[0-9]+(?:\\.[0-9]+)?"),])
    //pattern found on: https://stackoverflow.com/questions/47030843/regex-pattern-for-period-separator-and-no-comma-angular-2
  });

  constructor(private  mapboxService: MapboxService, private genreService: GenreService, private eventService: EventService,
              private  jwtService: JwtService, private router: Router, private route: ActivatedRoute) { }

  compareFunction(o1: any, o2: any) {
    return (o1 == o2);
  }


  ngOnInit(): void {

    if (typeof(window) !== 'undefined'){
      //If user is not authenticated user cannot access the page
      if (!this.jwtService.isAuthenticated()){
        this.router.navigateByUrl('').then();
      }

      const token = JWT(this.jwtService.getAuthToken());
      //gets all genres of event-details and displays it
      this.sub = this.genreService.getAllGenres().subscribe(
          data => this.genres = <Genre[]> data['category'],
          error => this.errorMessage
      );

      this.sub = this.eventService.getEvent(this.getIdInURL()).subscribe(
          data =>{
            //Controls if the user that is trying to edit the event is either an admin or the creator the event
            if (data['creator'] !== token.id){
              if (token.role !== 'admin'){
                this.router.navigateByUrl('').then();
              }
            }

            //Set Event data into form
            this.editEvent.controls['title'].setValue(data['title']);
            this.editEvent.controls['description'].setValue(data['description']);
            this.editEvent.controls['address'].setValue(data['address']);
            const setStartDate = new Date(data['startDate']);
            const formatted_startTime = setStartDate.getHours() + ':' + setStartDate.getMinutes();
            this.editEvent.controls['startDate'].setValue(setStartDate);
            this.editEvent.controls['startTime'].setValue(formatted_startTime);

            const setEndDate = new Date(data['endDate']);
            const formatted_endTime = setEndDate.getHours() + ':' + setEndDate.getMinutes();
            this.editEvent.controls['endDate'].setValue(setEndDate);
            this.editEvent.controls['endTime'].setValue(formatted_endTime);
            this.editEvent.controls['price'].setValue(data['price']);
            this.editEvent.controls['genre'].setValue(data['category']);
            this.image = data['image'];


            const coordinates = [ data['longitude'], data['latitude']];
            this.selectedAddress = coordinates;

          }
      )
    }
  }

  //gets the id of the event the user is trying to edit
  private getIdInURL(): number {
    return +this.route.snapshot.paramMap.get('id');
  }

  //shows the user a message
  private setMessage(error: any): void {
    this.errorMessage = error.message;
  }

  //Creates the event-details
  public update(): void {
    if (this.editEvent.valid){

      // Gets both end and start time of the event-details
      const startTime = this.editEvent.get('startTime').value;
      const endTime = this.editEvent.get('endTime').value;

      this.errorMessage = '';

      // converts the time into hours and minutes
      const startTimeHours = parseInt(startTime.slice(0, 2), 10);
      const startTimeMinutes = parseInt(startTime.slice(3, 5), 10);
      const endTimeHours = parseInt(endTime.slice(0, 2), 10);
      const endTimeMinutes = parseInt(endTime.slice(3, 5), 10);

      // creates the date of the start and end of the event-details with the time
      const endDate = new Date(this.editEvent.get('endDate').value);
      endDate.setHours(endTimeHours, endTimeMinutes, 0, 0);
      const startDate = new Date(this.editEvent.get('startDate').value);
      startDate.setHours(
          startTimeHours,
          startTimeMinutes,
          0,
          0,
      );

      //Control if the dates are correct
      if (endDate <= startDate) {
        this.errorMessage = 'Check your dates!';
        return;
      }

      //Formats the dates to send to the server
      const formatted_startDate =
          startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate() +  ' '
          + startDate.getHours() + ':' + startDate.getMinutes() + ':' + startDate.getSeconds();

      const formatted_endDate =
          endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate() +  ' '
          + endDate.getHours() + ':' + endDate.getMinutes() + ':' + endDate.getSeconds();

      //Object that will be sent to the server
      let data: { [k: string]: any } = {};

      const form = new FormData();
      form.append("id", this.getIdInURL().toString());
      form.append("title", this.editEvent.get('title').value);
      form.append("startDate", formatted_startDate);
      form.append("endDate", formatted_endDate);
      if (this.editEvent.get('price').value !== ''){
        form.append("price", this.editEvent.get('price').value);
      }
      form.append("address", this.editEvent.get('address').value);
      form.append("description",  this.editEvent.get('description').value);
      form.append("longitude", this.selectedAddress[0]);
      form.append("latitude", this.selectedAddress[1]);
      if (this.fileToUpload !== null){
        form.append("image", this.fileToUpload, this.fileToUpload.name);
      }
      form.append("category", this.editEvent.get('genre').value);

      this.errorMessage = "Updating...";
      // sends data
      this.sub = this.eventService.updateEvent(form).subscribe(
          data => this.setMessage(data),
          error => this.errorMessage = 'Error occurred while updating your event-details, please try again later.',
      );
    }
  }


  //Function that search for the address when user types a word
  public search(event: any): void{
    const searchTerm = event.toLowerCase();

    if (searchTerm && searchTerm.length > 0){
      this.sub = this.mapboxService.search_word(searchTerm).subscribe(
          data => {
            this.addresses = <MapBoxAPI> data},
          error => {
            this.errorMessage = 'Error loading addresses, please try again later.'
          },
      );
      //this.addresses = this.addresses['features'];
    }
  }

  //Saved the address that the user selected
  public onSelect(address: any): void{
    this.selectedAddress = address.geometry.coordinates;
  }


  //Gets the file selected
  public onFileSelected(event: any):void {
    this.fileToUpload = event.target.files[0];
    this.fileMessage = this.fileToUpload.name
  }


/*  ngOnDestroy() {
    //Destroys every subscription to free up RAM
    this.sub.unsubscribe();
  }*/
}
