import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {CookieService} from "ngx-cookie";

@Component({
  selector: 'app-logout',
  templateUrl: './logout.component.html',
  styleUrls: ['./logout.component.scss']
})
export class LogoutComponent implements OnInit {


  constructor(private cookieService: CookieService, private router: Router) { }
//Destroys the cookies the user has and redirects him to the main page
  ngOnInit() {
    this.cookieService.remove('LuxEvent');
    setTimeout(() => {
      this.router.navigateByUrl('/').then();
    }, 100);
  }

}
