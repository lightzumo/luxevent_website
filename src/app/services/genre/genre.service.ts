import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  _url: string;
  constructor(private _http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  //fetches all genres
  public getAllGenres(){
    this._url = 'genre/all';
    return this._http.get(this._url, this.httpOptions);
  }

  //creates a genre
  public createGenre(genreData){
    this._url = 'genre/manage/create';
    return this._http.post(this._url, genreData, this.httpOptions);
  }
}
