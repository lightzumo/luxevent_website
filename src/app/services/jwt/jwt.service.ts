import { Injectable } from '@angular/core';
import {CookieService} from "ngx-cookie";

@Injectable({
  providedIn: 'root'
})
export class JwtService {

  constructor(private cookieService: CookieService) { }

  public isAuthenticated(): boolean {
    return !(this.cookieService.get('LuxEvent') === null || this.cookieService.get('LuxEvent') === undefined);

  }

  public getAuthToken(): string {
    return this.cookieService.get('LuxEvent');
  }
}
