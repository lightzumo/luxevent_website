import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  _url: string;
  constructor(private _http: HttpClient) { }

  //http options that will be set in the header
  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  //Creates an account
  public register(regForm: any) {
    this._url = "/auth/register";
    return this._http.post(this._url, regForm.value, this.httpOptions);
  }

  //logs the user in
  public login(email: string, password: string) {
    this._url = '/auth/login';
    const loginCredentials: object = {
      'email': email,
      'password': password
    };
    return this._http.post<object>(this._url, loginCredentials, this.httpOptions);

  }
}
