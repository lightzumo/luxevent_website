import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class EventService {
  _url: string;

  constructor(private _http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  //create event-details request
  public createEvent(data: any){
    this._url = 'event/manage/create';
    return  this._http.post(this._url, data, { headers: null });
  }

  //create event-details request
  public getEvent(id: number){
    this._url = 'event/get/' + id;
    return  this._http.get(this._url, this.httpOptions);
  }

  //Request that checks if user attends an event
  public doesUserLikeEvent(eventId: number){
    this._url = 'event/user/like/'+eventId;
    return this._http.get(this._url, this.httpOptions);
  }

  //request that updates if user is attending or not attending an event
  public updateUserLikeEvent(eventId: number){
    this._url = 'event/user/like';
    return this._http.post(this._url, JSON.stringify({event: eventId}), this.httpOptions);
  }

  //search event by date and genre
  public searchEvents(date: string, genre: number){
    this._url = 'event/search?date='+date+'&genre='+genre;
    return this._http.get(this._url, this.httpOptions);
  }

  //search events by date
  public searchEventsByDate(date: string){
    this._url = 'event/searchByDate?date='+date;
    return this._http.get(this._url, this.httpOptions);
  }

  //gets all events that user created
  public getAllUserEvents(){
    this._url = 'event/my';
    return this._http.get(this._url, this.httpOptions);
  }

  //gets all events
  public getAllEvents(){
    this._url = 'event/all';
    return this._http.get(this._url, this.httpOptions);
  }

  //gets all events that user will attend
  public getAllUserAttendEvents(){
    this._url = 'event/attend';
    return this._http.get(this._url, this.httpOptions);
  }

  //updates an event
  public updateEvent(data: any){
    this._url = 'event/manage/edit';
    return  this._http.post(this._url, data, { headers: null });
  }


}
