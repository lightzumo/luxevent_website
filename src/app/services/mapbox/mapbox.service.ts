import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class MapboxService {

  constructor(private _http: HttpClient) { }

  //Searches addresses with the text written by the user
  search_word(query: string){
    const url = 'https://api.mapbox.com/geocoding/v5/mapbox.places/';
    return this._http.get(url + query + '.json?types=address&country=LU&access_token=pk.eyJ1IjoibGlnaHR6dW1vIiwiYSI6ImNrNzRlenJzeTAzZGEzaG12OXZoaW5xNnAifQ.5-sr33m4eHL2-Cdx-01Oig')
  }

}
