import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  _url: string;

  constructor(private _http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    })
  };

  //get all users details
  public getAllUsers(){
    this._url = 'user/manage/all';
    return  this._http.get(this._url, this.httpOptions);
  }

  //get a specific user details
  public getUser(id: number){
    this._url = 'user/manage/get/'+id;
    return  this._http.get(this._url, this.httpOptions);
  }


  //update user
  public updateUser(id: number, data: any){
    this._url = 'user/manage/edit/'+id;
    return  this._http.post(this._url,data,this.httpOptions);
  }
}
